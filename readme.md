# Instalar Pacotes

No terminal da pasta raiz do projeto, digite o seguinte comando:

```javascript
npm install
```

Com este comando irá instalar todos os pacotes que foi utilizado no desenvolvimento.

# Configuração da API

Alterar a URL do seu banco de dados, o usuario e senha no arquivo app.js
(MongoDB)

```javascript
                    //URL
mongoose.connect('mongodb+srv://USER:PASSWORD@tool.ibww1.mongodb.net/tool?retryWrites=true&w=majority');
```

# Uso da API

Para executar a api, digite o seguinte comando no terminal da pasta raiz:

```javascript
nodemon bin/server.js
```

Exemplo de retorno:

```javascript
[nodemon] 2.0.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json  
[nodemon] starting `node bin/server.js`     
Servidor rodando na porta 3000
```

## Endpoints

## Produtos

Cadastrar um Produto

> POST
>
> /products

Exemplo de requisição:

```json
{
    "name": "iPhone X",
    "sku": 789,
    "price": 2999.99,
    "description": "Um iPhone X",
    "quantity": 15,
    "category": [
        "iphone",
        "cell"
    ]
}
```

Exemplo de retorno:

```json
{
    "message": "Created!",
    "Product": {
        "name": "iPhone X",
        "sku": 789,
        "price": 2999.99,
        "description": "Um iPhone X",
        "quantity": 15,
        "category": [
            "iphone",
            "cell"
        ]
    }
}
```

Listar todos os produtos cadastrados

> GET
>
> /products

Exemplo de retorno:

```json
[
    {
        "_id": "61ede518e052c79253ae2ba2",
        "name": "iPhone X",
        "sku": 789,
        "price": 2999.99,
        "description": "Um iPhone X",
        "quantity": 15,
        "category": [
            "iphone",
            "cell"
        ]
    },
    {
        "_id": "61ede9d88973a7eb3202fc02",
        "name": "Xiaomi Redmi Note 9s",
        "sku": 459,
        "price": 1599.99,
        "description": "Um Xiaomi Redmi Note 9s",
        "quantity": 15,
        "category": [
            "xiaomi",
            "cell"
        ]
    }
]
```

Listar produtos conforme uma tag inserida

> GET
>
> /products/{iphone} <- Exemplo

Exemplo de retorno:

```json
[
     {
        "_id": "61ede518e052c79253ae2ba2",
        "name": "iPhone X",
        "sku": 789,
        "price": 2999.99,
        "description": "Um iPhone X",
        "quantity": 15,
        "category": [
            "iphone",
            "cell"
        ]
    },
    {
        "_id": "61ede9d88973a7eb3202fc02",
        "name": "iPhone XS",
        "sku": 729,
        "price": 4999.99,
        "description": "Um iPhone XS",
        "quantity": 15,
        "category": [
            "iphone",
            "cell"
        ]
    }
]
```

Alterar alguma informação de um produto cadastrado

> PUT
>
> /products/{id} <- Id do produto que deseja fazer alguma alteração

Exemplo de requisição:

```json
{
    "name": "Xiaomi Redmi Note 10s",
    "sku": 789,
    "price": 1899.99,
    "description": "Um Xiaomi Redmi Note 10s",
    "quantity": 15,
    "category": [
        "xiaomi",
        "cell"
    ]
}
```

Exemplo de retorno:

```json
{
    "message": "Successfully updated!",
    "Product": {
        "name": "Xiaomi Redmi Note 10s",
        "sku": 789,
        "price": 1899.99,
        "description": "Um Xiaomi Redmi Note 10s",
        "quantity": 15,
        "category": [
            "xiaomi",
            "cell"
        ]
    }
}
```

Deletar um produto pelo id

> DELETE
>
> /products/{id} <- Id do produto que deseja deletar

Exemplo de retorno:

```json
{
    "message": "Successfully deleted!"
}
```

## Categorias

Cadastrar uma categoria

> POST
>
> /categories

Exemplo de requisição:

```json
{
    "name": "Cell",
    "code": 12
}
```

Exemplo de retorno:

```json
{
    "message": "Created!",
    "Category": {
        "name": "Cell",
        "code": 12
    }
}
```

Listar todas as categorias cadastradas

> GET
>
> /categories

Exemplo de retorno:

```json
[
    {
        "_id": "61ede4e2e052c79253ae2ba0",
        "name": "Cell",
        "code": 12
    },
    {
        "_id": "61edec60ff013a215c165d1f",
        "name": "Notebook",
        "code": 15
    }
]
```

Alterar alguma informação de uma categoria cadastrada

> PUT
>
> /categories/{id} <- Id da categoria que deseja fazer alguma alteração

Exemplo de requisição:

```json
{
    "name": "Notebook",
    "code": 15
}
```

Exemplo de retorno:

```json
{
    "message": "Successfully updated!",
    "Category": {
        "name": "Notebook",
        "code": 15
    }
}
```

Deletar uma categoria pelo id

> DELETE
>
> /categories/{id} <- Id da categoria que deseja deletar

Exemplo de retorno:

```json
{
    "message": "Successfully deleted!"
}
```
