'use strict';

const mongoose = require("mongoose");
const schema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  code: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('Categories', schema);