'use strict'

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = express();

mongoose.connect('mongodb+srv://USER:PASSWORD@products.ibww1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');
const Product = require("./models/product");
const Category = require("./models/category");

const indexRoute = require("./routes/index-route");
const productRoute = require("./routes/products-route");
const categoryRoute = require("./routes/categories-route");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRoute);
app.use('/products', productRoute);
app.use('/categories', categoryRoute);

module.exports = app;