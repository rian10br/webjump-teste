'use strict';

const mongoose = require("mongoose");
const Products = mongoose.model('Products');

exports.post = (req, res, next) => {
  var product = new Products(req.body);
  product.save().then(x => {
    res.status(201).send({
      message: "Created!",
      Product: req.body
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};

exports.get = (req, res, next) => {
  Products.find({}, 'name sku price description quantity category').then(data => {
    res.status(200).send(data);
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
}

exports.getByCategory = (req, res, next) => {
  Products.find({ category: req.params.category }, 'name sku price description quantity category').then(data => {
    res.status(200).send(data);
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
}

exports.put = (req, res, next) => {
  Products.findByIdAndUpdate(req.params.id, {
    $set: {
      name: req.body.name,
      sku: req.body.sku,
      price: req.body.price,
      description: req.body.description,
      quantity: req.body.quantity,
      category: req.body.category,
    }
  }).then(x => {
    res.status(200).send({
      message: "Successfully updated!",
      Product: req.body
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};

exports.delete = (req, res, next) => {
  Products.findOneAndRemove(req.params.id).then(x => {
    res.status(200).send({
      message: "Successfully deleted"
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};