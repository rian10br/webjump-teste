'use strict';

const mongoose = require("mongoose");
const Categories = mongoose.model('Categories');

exports.post = (req, res, next) => {
  var category = new Categories(req.body);
  category.save().then(x => {
    res.status(201).send({
      message: "Created!",
      Category: req.body
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};

exports.get = (req, res, next) => {
  Categories.find({}, 'name code').then(data => {
    res.status(200).send(data);
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
}

exports.put = (req, res, next) => {
  Categories.findByIdAndUpdate(req.params.id, {
    $set: {
      name: req.body.name,
      code: req.body.code
    }
  }).then(x => {
    res.status(200).send({
      message: "Successfully updated!",
      Category: req.body
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};

exports.delete = (req, res, next) => {
  Categories.findOneAndRemove(req.params.id).then(x => {
    res.status(200).send({
      message: "Successfully deleted"
    });
  }).catch(e => {
    res.status(400).send({
      message: "Something went wrong!",
      data: e
    });
  });
};